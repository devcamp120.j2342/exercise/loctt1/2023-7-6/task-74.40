-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2023 at 03:41 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task74.40`
--

-- --------------------------------------------------------

--
-- Table structure for table `drinks`
--

CREATE TABLE `drinks` (
  `id` int(11) NOT NULL,
  `drink_code` text NOT NULL,
  `drink_name` text NOT NULL,
  `total_money` int(11) NOT NULL,
  `create_date` int(11) NOT NULL,
  `update_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `drinks`
--

INSERT INTO `drinks` (`id`, `drink_code`, `drink_name`, `total_money`, `create_date`, `update_date`) VALUES
(1, 'SE_1701', 'Trà tắc', 15000, 15012023, 15022023),
(2, 'SE_1702', 'Trà đào', 20000, 16012023, 17012023),
(3, 'SE_1703', 'Trà Sữa', 20000, 15012023, 160120203),
(4, 'SE_1704', 'Cà Phê Sữa', 20000, 16012023, 17012023),
(5, 'SE_1705', 'Capuchino', 25000, 2012023, 3012023);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `oder_code` text NOT NULL,
  `pizza_size` text NOT NULL,
  `pizza_type` text NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `total-money` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `email` text NOT NULL,
  `address` text NOT NULL,
  `drink_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `create_date` int(11) NOT NULL,
  `update_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `oder_code`, `pizza_size`, `pizza_type`, `voucher_id`, `total-money`, `discount`, `email`, `address`, `drink_id`, `status_id`, `create_date`, `update_date`) VALUES
(1, 'SE_1701', 'S', 'Pizza hải sản', 1, 150000, 10, 'loc@gmail.com', '115 Dương Minh Quang', 1, 2, 716398127, 716712938),
(2, 'SE-1702', 'M', 'Pizza popcorn', 2, 200000, 10, 'cam@gmail.com', '322 Dương Minh Quang', 2, 2, 871528371, 781623123),
(3, 'SE_1703', 'L', 'Pizza phô mai', 2, 200000, 20, 'huyen@gmail.com', '182 Nguyễn Văn Linh', 3, 3, 871623, 712398),
(4, 'SE_1704', 'S', 'Pizza popcorn', 4, 150000, 20, 'my@gmail.com', '600 Nguyễn Văn Cừ', 4, 4, 152837, 718627),
(5, 'SE_1705', 'L', 'Pizza hải sản', 5, 200000, 20, 'day@gmail.com', '573 Lê Quý Đôn', 5, 5, 817263, 712726);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `status_code` text NOT NULL,
  `status_name` text NOT NULL,
  `create_date` int(11) NOT NULL,
  `update_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status_code`, `status_name`, `create_date`, `update_date`) VALUES
(1, 'PE_1701', 'Đang Giao', 54215162, 76198316),
(2, 'PE_1702', 'Đã nhận đơn', 718623172, 671526371),
(3, 'PE_1703', 'Đã giao', 17923612, 71263123),
(4, 'PE_1704', 'Đã giao', 17836131, 78639812),
(5, 'PE_1705', 'Đang giao', 1623162, 1762889);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_name` text NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `create_date` int(11) NOT NULL,
  `update_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `first_name`, `last_name`, `email`, `password`, `create_date`, `update_date`) VALUES
(1, 'loctt', 'Lộc', 'Tấn', 'loc@gmail.com', '123456789', 76125871, 69864368),
(2, 'camvtm', 'Cẩm', 'Vương', 'cam@gmail.com', '0949711612', 298347, 892171),
(3, 'huyenmy', 'Huyền', 'Mỹ', 'huyen@gmail.com', '31318411654', 782630918, 615238716),
(4, 'myblh', 'Mỹ', 'Hoàn', 'my@gmail.com', '3186131831681', 18736123, 61523912),
(5, 'Daytn', 'Đầy', 'Ngọc', 'day@gmail.com', '543118613', 15283761, 15238123);

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL,
  `voucher_code` text NOT NULL,
  `discount` int(11) NOT NULL,
  `is_used_yn` text NOT NULL,
  `create_date` int(11) NOT NULL,
  `update_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `voucher_code`, `discount`, `is_used_yn`, `create_date`, `update_date`) VALUES
(1, 'FE-1701', 20, '', 69381294, 87152836),
(2, 'FE-1702', 10, '', 7162938, 1285387),
(3, 'FE-1703', 20, '', 8712639, 1872630),
(4, 'FE-1704', 30, '', 7634324, 8736923),
(5, 'FE-1705', 10, '', 12238713, 78162981);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD KEY `voucher_id` (`voucher_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `drink_id` (`drink_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_name` (`user_name`) USING HASH,
  ADD UNIQUE KEY `email` (`email`) USING HASH;

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`voucher_id`) REFERENCES `vouchers` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`drink_id`) REFERENCES `drinks` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
